import javafx.util.Pair;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GenericNestedList<E> implements Iterable<Pair<E, Integer>>, NestedList<E> {

    private E baseValue;
    private List<NestedList<E>> list;
    private int idx = 0;

    public GenericNestedList(E baseValue) {
        this.baseValue = baseValue;
    }

    public GenericNestedList(List<NestedList<E>> list){
        this.list = list;
    }

    @Override
    public boolean isBase() {
        return baseValue != null;
    }

    @Override
    public E getBaseValue() {
        if (!isBase())
            throw new IllegalStateException("Not a base case.");
        return this.baseValue;
    }

    public void setBaseValue(E value) {
        this.baseValue = value;
    }

    @Override
    public List<NestedList<E>> getList() {
        if (isBase())
            throw new IllegalStateException("Not a recursive case.");
        return this.list;
    }

    public void setList(List<NestedList<E>> list) {
        this.list = list;
    }

    static <E> void walkNested(NestedList<E> list, int depth) {
        if (list.isBase())
            System.out.printf("%d: %s\n", depth, list.getBaseValue().toString());
        else {
            for (NestedList<E> member: list.getList())
                walkNested(member, depth + 1);
        }
    }

    public static void main(String[] args) {
        GenericNestedList<Integer> base1 = new GenericNestedList<>(3);
        GenericNestedList<Integer> base2 = new GenericNestedList<>(7);
        GenericNestedList<Integer> base3 = new GenericNestedList<>(10);
        GenericNestedList<Integer> base4 = new GenericNestedList<>(20);
        GenericNestedList<Integer> second1 = new GenericNestedList<>(Arrays.asList(base1, base2));
        GenericNestedList<Integer> second2 = new GenericNestedList<>(Arrays.asList(base3, base4));
        GenericNestedList<Integer> second3 = new GenericNestedList<>(5);
        GenericNestedList<Integer> second4 = new GenericNestedList<>(Arrays.asList(base2, base3, base1));
        GenericNestedList<Integer> first1 = new GenericNestedList<>(Arrays.asList(second1, second3));
        GenericNestedList<Integer> first2 = new GenericNestedList<>(Arrays.asList(second4, second2));
        GenericNestedList<Integer> zero = new GenericNestedList<>(Arrays.asList(first1, first2));
        for (Pair<Integer, Integer> e: zero) {
            System.out.println(e);
        }
        System.out.println("=================");
        walkNested(zero, 0);
    }

    private class GenericIterator implements Iterator<Pair<E, Integer>> {

        @Override
        public boolean hasNext() {
            if (isBase())
                return false;
            return idx < list.size();
        }

        Pair<E, Integer> walk(GenericNestedList<E> list, int depth) {
            Pair<E, Integer> ret = null;
            while (list.idx < list.getList().size()) {
                GenericNestedList<E> member = (GenericNestedList<E>) list.getList().get(list.idx);
                if (member.isBase()) {
                    list.idx++;
                    ret = new Pair<>(member.getBaseValue(), depth + 1);
                } else {
                    ret = walk(member, depth + 1);
                }

                if (ret != null) {
                    return ret;
                } else {
                    list.idx++;
                }
            }
            return null;
        }

        @Override
        public Pair<E, Integer> next() {
            return walk(GenericNestedList.this, 0);
        }
    }

    @Override
    public Iterator<Pair<E, Integer>> iterator() {
        return new GenericIterator();
    }
}
