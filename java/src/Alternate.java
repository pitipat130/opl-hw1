import java.util.*;
import java.util.function.Consumer;

public class Alternate<E> implements Iterable<E> {

    private final Iterator<E> iter1;
    private final Iterator<E> iter2;
    private final Iterator<E>[] iters;

    public Alternate(Iterable<E> it1, Iterable<E> it2) {
        this.iter1 = it1.iterator();
        this.iter2 = it2.iterator();
        this.iters = (Iterator<E>[]) new Iterator[2];
        this.iters[0] = iter1;
        this.iters[1] = iter2;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int turn = 0;

            @Override
            public boolean hasNext() {
                return iter1.hasNext() || iter2.hasNext();
            }

            @Override
            public E next() throws NoSuchElementException {
                while (hasNext()) {
                    if (iters[turn].hasNext()) {
                        E ret = iters[turn].next();
                        turn ^= 1;
                        return ret;
                    } else {
                        turn ^= 1;
                    }
                }
                throw new NoSuchElementException("no more elements");
            }
        };
    }

    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<Integer>() {{
            add(1); add(2); add(3); add(4); add(10); add(20); add(30);
        }};
        List<Integer> list2 = new ArrayList<Integer>() {{
            add(5); add(6); add(7); add(8);
        }};

        Iterable<Integer> iter = new Alternate<>(list1, list2);
        for (Integer e : iter){
            System.out.println(e);
        }
    }
}
