def stitch_files(*file_names):
    for file_name in file_names:
        if type(file_name) != str:
            raise ValueError(f"Invalid parameter type of {file_name}. Must be str.")
        with open(file_name, 'r') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                yield line.rstrip()

