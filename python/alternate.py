def iter_alternate(iter1, iter2):
    turn = 0
    iters = [iter(iter1), iter(iter2)]
    while True:
            try:
                yield next(iters[turn])
                turn ^= 1
            except StopIteration:
                turn ^= 1
                continue
