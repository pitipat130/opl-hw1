def memoized(func):
    mem = {}

    def wrapper(*args):
        if args in mem:
            return mem[args]
        else:
            mem[args] = func(*args)
            return mem[args]

    return wrapper


@memoized
def fib(n):
    if n <= 1:
        return n
    else:
        return fib(n - 1) + fib(n - 2)


@memoized
def n_choose_r(n, r):
    if n < r:
        return 0
    if r == 0:
        return 1
    else:
        return n_choose_r(n - 1, r - 1) + n_choose_r(n - 1, r)


