def all_perms(n, lst=None):
    if lst is None:
        lst = list(range(1, n+1))
    if n == 1:
        yield tuple([lst[0]])
    else:
        for i in range(n):
            tmp = lst[:]
            del tmp[i]
            for x in all_perms(n-1, tmp):
                ret = [lst[i]]
                ret.extend(list(x))
                yield tuple(ret)
