from itertools import count


def iter_dovetail(*iterators):
    iters = [iter(i) for i in iterators]
    for i in count():
        found = 0
        for j in range(i, -1, -1):
            if j < len(iters):
                try:
                    yield next(iters[j])
                    found += 1
                except StopIteration:
                    continue
        if found == 0:
            break

